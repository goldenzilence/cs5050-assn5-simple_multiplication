#pragma once

#include <vector>
#include "Timer.hpp"
class Multiplier {
public:
    Multiplier(std::vector<double> first_polynomial, std::vector<double> second_polynomial);

	std::vector<double> product();
	double time_taken() { return timer.duration(); }
	size_t max_order();
private:
	std::vector<double> P;
	std::vector<double> Q;
	Timer timer;
};