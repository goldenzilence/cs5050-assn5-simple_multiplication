//
// Created by Aaron Fine on 9/28/2018.
//

#include <iomanip>
#include <sstream>
#include "Timer.hpp"

Timer::Timer()
{
    m_duration = std::chrono::duration<double>::zero();
}

void Timer::start()
{
    m_startTime = std::chrono::high_resolution_clock::now();
    timerRunning = true;
}

void Timer::stop()
{
    if (timerRunning)
    {
        m_duration += std::chrono::high_resolution_clock::now() - m_startTime;
        timerRunning = false;
    }
}

double Timer::duration()
{
    return std::chrono::duration_cast<std::chrono::microseconds>( m_duration ).count() / static_cast<double>(1000000);
}

std::string Timer::displayDuration( int precision )
{
    std::stringstream output;
    output << std::fixed << std::setprecision( precision );
    output << "Time spent calculating product: " << duration() << " seconds\n";
    return output.str();
}