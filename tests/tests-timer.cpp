//
// Created by Aaron Fine on 9/29/2018.
//

#include<catch2/catch.hpp>
#include "Timer.hpp"

#include <chrono>
#include <thread> // sleep code from https://stackoverflow.com/a/10613664/8223925
#include <iostream>


TEST_CASE("Test the timer class", "[Timer]")
{
    Timer timer;
    REQUIRE(timer.duration() == 0);

    SECTION("Starting and stopping a timer")
    {
        timer.start();
        timer.stop();
        REQUIRE(timer.duration() == Approx(0).margin(0.005));
    }

    SECTION("Running timer for 1/2 second")
    {
        timer.start();
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        timer.stop();

        REQUIRE(timer.duration() == Approx(0.5).margin(0.005));
    }

    SECTION("Running timer for 1 second")
    {
        timer.start();
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        timer.stop();

        REQUIRE(timer.duration() == Approx(1.0).margin(0.005));
    }

    SECTION("Running timer for 5 second")
    {
        timer.start();
        std::this_thread::sleep_for(std::chrono::milliseconds(5000));
        timer.stop();

        REQUIRE(timer.duration() == Approx(5.0).margin(0.005));
    }

    SECTION("Accumulating time over multiple starts and stops")
    {
        timer.start();
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        timer.stop();

        timer.start();
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        timer.stop();

        timer.start();
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        timer.stop();

        REQUIRE(timer.duration() == Approx(1.5).margin(0.005));
    }

}