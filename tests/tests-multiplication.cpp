#include <vector>
#include<catch2/catch.hpp>

#include "Multiplier.hpp"

TEST_CASE("Basic Products are computed", "{Multiplier}") {

	Multiplier first({ 1, 2 }, { 2, 1 });
	std::vector<double> a1{ 2, 5, 2 };
	REQUIRE(first.product() == a1);

	Multiplier second({ 1, 2 }, { 1, 2 });
	std::vector<double> a2{ 1, 4, 4 };
	REQUIRE(second.product() == a2);

	Multiplier third({ 1, 2 }, { 3, 4 });
	std::vector<double> a3{ 3, 10, 8 };
	REQUIRE(third.product() == a3);

	Multiplier fourth({ 0, 1 }, { 0, 1 });
	std::vector<double> a4{ 0, 0, 1 };
	REQUIRE(fourth.product() == a4);

	Multiplier fifth({ 1 }, { 2 });
	std::vector<double> a5{ 2 };
	REQUIRE(fifth.product() == a5);
}

TEST_CASE("Negative Products are computed", "{Multiplier}") {

	Multiplier first({ -1, -1 , -1, -1}, { -2, -2, -2, -2 });
	std::vector<double> a1{ 2, 4, 6, 8, 6, 4, 2 };
	REQUIRE(first.product() == a1);

	Multiplier second({ -1, -1 , -1, -1 }, { 2, 2, 2, 2 });
	std::vector<double> a2{ -2, -4, -6, -8, -6, -4, -2 };
	REQUIRE(second.product() == a2);

	Multiplier third({ -1, 1 , -1, 1 }, { 2, -2, 2, -2 });
	std::vector<double> a3{ -2, 4, -6, 8, -6, 4, -2 };
	REQUIRE(third.product() == a3);
}

TEST_CASE("Fractional Products are computed", "{Multiplier}") {

	Multiplier first({ 0.5, 0.5 }, { 1, 1 });
	std::vector<double> a1{ 0.5, 1, 0.5 };
	REQUIRE(first.product() == a1);
}