#include "Multiplier.hpp"
#include <iostream>
#include <random>
#include <fstream>
#include <iomanip>

std::vector<double> generate_polynomial(size_t order)
{
	// Reference: https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::vector<double> output(order, 0);
	std::uniform_real_distribution<> dis(-1.0, 1.0);
	for (size_t i = 0; i < order; i++)
	{
		output[i] = dis(gen);
	}

	return output;
}


int main()
{
	size_t upper_limit = std::pow(2, 20);
	size_t lower_limit = std::pow(2, 5);
	
	std::ofstream fout("data.dat");
	
	std::cout << std::fixed << std::setprecision(7);
	fout << std::fixed << std::setprecision(7);

	std::cout << std::setw(12) << "Order (n)" << '|' << std::setw(13) << "Total (Sec)" << std::setw(13) << "Ave (Sec)" << std::endl;
	fout << "Order (n)" << ',' << "Total (Sec)" << ',' << "Ave (Sec)" << std::endl;
	size_t num_repeats = 10;
	
	Timer local_timer;
	local_timer.start();

	for (size_t i = lower_limit; i < upper_limit; i *= 2)
	{
		
		double total = 0;
		for (size_t repeat = 0; repeat < num_repeats; repeat++)
		{
			std::vector<double> first = generate_polynomial(i);
			std::vector<double> second = generate_polynomial(i);
			Multiplier test(first, second);
			test.product();
			total += test.time_taken();
		}
		
		fout << i << ',' << total << ',' << total / num_repeats << '\n';
		std::cout << std::setw(12) << i << '|' << std::setw(13) << total << '|' << std::setw(13) << total / num_repeats <<std::endl;
	}
	local_timer.stop();
	std::cout << "Overall total time: " << local_timer.duration() << " seconds or " << local_timer.duration() / 60 << " minutes.";
	fout.close();
    return EXIT_SUCCESS;
}