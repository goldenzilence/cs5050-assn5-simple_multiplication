cmake_minimum_required(VERSION 3.8)

project(Assignment5 LANGUAGES CXX VERSION 0.0.1)

find_package(Catch2 REQUIRED)

set(source_files
   Multiplier.hpp
   Multiplier.cpp
   Timer.hpp
   Timer.cpp
)

add_library(BasicMultiplication STATIC ${source_files})
target_compile_features(BasicMultiplication PUBLIC cxx_std_11)
target_include_directories(BasicMultiplication PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} BasicMultiplication)

add_subdirectory(tests)