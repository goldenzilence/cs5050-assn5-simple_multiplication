#pragma once

#include <chrono>
#include <string>

class Timer 
{
private:
    std::chrono::duration<double> m_duration;
    std::chrono::time_point<std::chrono::high_resolution_clock> m_startTime;
    bool timerRunning = false;

public:
    Timer();

    void start();

    void stop();

    double duration();

    std::string displayDuration( int precision = 4 );

};