#include "Multiplier.hpp"
#include <algorithm>

Multiplier::Multiplier(std::vector<double> first_polynomial, std::vector<double> second_polynomial) :
	P(first_polynomial), Q(second_polynomial), timer()
{
	//Empty
}

std::vector<double> Multiplier::product()
{
	std::vector<double> output(max_order()+1, 0);
	timer.start();
	for (size_t i = 0; i < P.size(); i++)
	{
		for (size_t j = 0; j < Q.size(); j++)
		{
			output[i + j] = P[i] * Q[j] + output[i + j];
		}
	}
	timer.stop();
	return output;
}

size_t Multiplier::max_order()
{
	std::size_t order_of_P = P.size() - 1;
	std::size_t order_of_Q = Q.size() - 1;
	return order_of_P + order_of_Q;
}
